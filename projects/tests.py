import csv
import os

from django.test import TestCase, Client
from django.contrib.auth.models import User
from unittest import skip
from projects.models import ProjectCategory, Project, Task, TaskOffer, Team


# Create your tests here.
class ProjectTaskViewTestCase(TestCase):

    def setUp(self):
        category = ProjectCategory()
        category.name = 'Category 1'
        category.save()

        self.user1 = User.objects.create_user(
            username='User 1',
            email="user1@email.com",
            password="12341234"
        )
        self.user1.is_active = True
        self.user1.save()
        self.user1.refresh_from_db()

        self.user2 = User.objects.create_user(
            username='User 2',
            email="user2@email.com",
            password="12341234"
        )
        self.user2.is_active = True
        self.user2.save()
        self.user2.refresh_from_db()

        self.user3 = User.objects.create_user(
            username='User 3',
            email="user3@email.com",
            password="12341234"
        )
        self.user3.is_active = True
        self.user3.save()
        self.user3.refresh_from_db()

        self.project = Project()
        self.project.user = self.user1.profile
        self.project.category = category
        self.project.title = 'Project 1'
        self.project.description = 'Project of user 1'
        self.project.status = 'o'
        self.project.save()

        self.tasks = []
        self.tasks.append(Task())
        self.tasks[0].project = self.project
        self.tasks[0].title = 'Task 1'
        self.tasks[0].description = 'Description of Task 1'
        self.tasks[0].budget = 200
        self.tasks[0].save()

        self.tasks.append(Task())
        self.tasks[1].project = self.project
        self.tasks[1].title = 'Task 2'
        self.tasks[1].description = 'Description of Task 5'
        self.tasks[1].budget = 300
        self.tasks[1].save()

    ###########################################################################################################
    ###
    ###     TASK 2 statement coverage tests
    ###
    ###########################################################################################################

    def test_project_anonymous_user_get(self):

        c = Client()
        response = c.get('/projects/' + str(self.project.pk) + '/', follow=True)

        # check if the response is correct
        self.check_project_page_response(response)

    def test_project_user_task_offer(self):

        c = Client()
        response = self.submit_task_offer(c, 300)

        self.check_project_page_response(response)

        # check if offer is in database
        offer = TaskOffer.objects.get(task_id=self.tasks[0].pk)
        assert (offer.price == 300)
        assert (offer.title == 'Offer Title')
        assert (offer.description == 'Offer Description')
        assert (offer.status == 'p')

    def test_project_owner_task_response(self):

        # submit an offer so there is something to accept
        c = Client()
        self.submit_task_offer(c, 300)
        offer = TaskOffer.objects.get(task_id=self.tasks[0].pk)

        c2 = Client()

        # log the owner in
        self.login(c2, 'User 1', '12341234')

        # accept the offer
        response = self.submit_task_response(c2, 'a', offer.pk)

        self.check_project_page_response(response)

        # check if response is in database
        offer = TaskOffer.objects.get(task_id=self.tasks[0].pk)
        assert (offer.feedback == 'Response Feedback')
        assert (offer.status == 'a')

    def test_project_owner_status_change(self):

        c = Client()

        response = self.set_project_status(c, 'f')

        self.check_project_page_response(response)
        assert (response.context[2]['project'].status == 'f')

    def test_task_not_logged_in_get(self):

        c = Client()
        response = c.get('/projects/' + str(self.project.pk) + '/tasks/' + str(self.tasks[0].pk) + '/', follow=True)

        # The login view should be presented
        assert (response.redirect_chain[0][0] == '/user/login/?next=/projects/1/tasks/1/')
        assert (response.redirect_chain[0][1] == 302)
        assert ('LoginView' in str(response.context[1]['view']))

    def test_task_owner_get(self):

        c = Client()
        # log the user in
        self.login(c, 'User 1', '12341234')

        response = c.get('/projects/' + str(self.project.pk) + '/tasks/' + str(self.tasks[0].pk) + '/', follow=True)

        # Check the users permissions returned by get_user_task_permissions
        permissions = response.context[2]['user_permissions']
        assert (permissions['upload'] == True)
        assert (permissions['owner'] == True)
        assert (permissions['read'] == True)
        assert (permissions['modify'] == True)
        assert (permissions['write'] == True)

    def test_task_accepted_offerer_get(self):
        # submit an offer so there is something to accept
        c = Client()
        # submit the offer
        self.submit_task_offer(c, 300)
        offer = TaskOffer.objects.get(task_id=self.tasks[0].pk)

        c2 = Client()
        # accept the offer
        self.submit_task_response(c2, 'a', offer.pk)

        response = c.get('/projects/' + str(self.project.pk) + '/tasks/' + str(self.tasks[0].pk) + '/', follow=True)

        # Check the users permissions returned by get_user_task_permissions
        permissions = response.context[1]['user_permissions']
        assert (permissions['upload'] == True)
        assert (permissions['owner'] == False)
        assert (permissions['read'] == True)
        assert (permissions['modify'] == True)
        assert (permissions['write'] == True)

    def test_task_team_member_view_get(self):
        # create a team
        self.team = Team()
        self.team.task = self.tasks[1]
        self.team.write = False
        self.team.save()
        self.team.members.set([self.user1.profile, self.user3.profile])

        # submit an offer so there is something to accept
        c = Client()
        self.login(c, 'User 3', '12341234')
        response = c.get('/projects/' + str(self.project.pk) + '/tasks/' + str(self.tasks[1].pk) + '/', follow=True)

        # Check the users permissions returned by get_user_task_permissions
        permissions = response.context[1]['user_permissions']
        assert (permissions['view_task'] == True)
        assert (permissions['upload'] == False)
        assert (permissions['owner'] == False)
        assert (permissions['read'] == False)
        assert (permissions['modify'] == False)
        assert (permissions['write'] == False)

    def test_task_team_member_write_get(self):
        # create a team
        self.team = Team()
        self.team.task = self.tasks[1]
        self.team.write = True
        self.team.save()
        self.team.members.set([self.user1.profile, self.user3.profile])

        # submit an offer so there is something to accept
        c = Client()
        self.login(c, 'User 3', '12341234')
        response = c.get('/projects/' + str(self.project.pk) + '/tasks/' + str(self.tasks[1].pk) + '/', follow=True)

        # Check the users permissions returned by get_user_task_permissions
        permissions = response.context[1]['user_permissions']
        assert (permissions['view_task'] == True)
        assert (permissions['upload'] == True)
        assert (permissions['owner'] == False)
        assert (permissions['read'] == False)
        assert (permissions['modify'] == False)
        assert (permissions['write'] == False)

    def test_task_participant_read_get(self):
        self.tasks[1].read.set([self.user3.profile])
        self.task_participant_permission_check(True, False, False)

    def test_task_participant_modify_get(self):
        self.tasks[1].modify.set([self.user3.profile])
        self.task_participant_permission_check(False, True, False)

    def test_task_participant_write_get(self):
        self.tasks[1].write.set([self.user3.profile])
        self.task_participant_permission_check(False, False, True)

    def test_task_participant_read_modify_write_get(self):
        self.tasks[1].read.set([self.user3.profile])
        self.tasks[1].modify.set([self.user3.profile])
        self.tasks[1].write.set([self.user3.profile])
        self.task_participant_permission_check(True, True, True)

    ###########################################################################################################
    ###
    ###     TASK 3 boundary value tests
    ###
    ###########################################################################################################

    def test_task_offer_boundary_positive(self):
        c = Client()
        self.submit_task_offer(c, 300)

        # check if offer is in database
        offer = TaskOffer.objects.get(task_id=self.tasks[0].pk)
        assert (offer.price == 300)
        assert (offer.title == 'Offer Title')
        assert (offer.description == 'Offer Description')
        assert (offer.status == 'p')

    def test_task_offer_boundary_zero(self):
        c = Client()
        self.submit_task_offer(c, 0)

        # check if offer is in database
        offer = TaskOffer.objects.get(task_id=self.tasks[0].pk)
        assert (offer.price == 0)
        assert (offer.title == 'Offer Title')
        assert (offer.description == 'Offer Description')
        assert (offer.status == 'p')

    @skip("Not working yet")
    def test_task_offer_boundary_negative(self):
        c = Client()
        self.submit_task_offer(c, -300)

        # The offer must not be in the database
        self.assertRaises(TaskOffer.DoesNotExist, TaskOffer.objects.get, task_id=self.tasks[0].pk)

    def test_task_offer_boundary_none(self):
        c = Client()
        self.submit_task_offer(c, None)

        # The offer must not be in the database
        self.assertRaises(TaskOffer.DoesNotExist, TaskOffer.objects.get, task_id=self.tasks[0].pk)

    @skip("Not working yet")
    def test_task_offer_boundary_string(self):
        c = Client()
        self.submit_task_offer(c, '1234')

        # The offer must not be in the database
        self.assertRaises(TaskOffer.DoesNotExist, TaskOffer.objects.get, task_id=self.tasks[0].pk)

    ###########################################################################################################
    ###
    ###     TASK 3 output coverage tests
    ###
    ###########################################################################################################

    def test_task_accept_offer_output_accept(self):
        c = Client()
        # submit an offer
        self.submit_task_offer(c, 300)
        offer = TaskOffer.objects.get(task_id=self.tasks[0].pk)

        c2 = Client()
        # accept the offer
        response = self.submit_task_response(c2, 'a', offer.pk)
        self.check_project_page_response(response)
        offer = TaskOffer.objects.get(task_id=self.tasks[0].pk)

        assert (offer.status == 'a');

    def test_task_accept_offer_output_pending(self):
        c = Client()
        # submit an offer
        self.submit_task_offer(c, 300)
        offer = TaskOffer.objects.get(task_id=self.tasks[0].pk)

        c2 = Client()
        # accept the offer
        response = self.submit_task_response(c2, 'p', offer.pk)
        self.check_project_page_response(response)
        offer = TaskOffer.objects.get(task_id=self.tasks[0].pk)

        assert (offer.status == 'p');

    def test_task_accept_offer_output_decline(self):
        c = Client()
        # submit an offer
        self.submit_task_offer(c, 300)
        offer = TaskOffer.objects.get(task_id=self.tasks[0].pk)

        c2 = Client()
        # accept the offer
        response = self.submit_task_response(c2, 'd', offer.pk)
        self.check_project_page_response(response)
        offer = TaskOffer.objects.get(task_id=self.tasks[0].pk)

        assert (offer.status == 'd');

    def test_task_accept_offer_output_not_existing_offer(self):
        c = Client()
        # submit an offer
        self.submit_task_offer(c, 300)
        TaskOffer.objects.get(task_id=self.tasks[0].pk)

        c2 = Client()
        # accept a not existing offer
        response = self.submit_task_response(c2, 'a', 1234)

        assert (response.status_code == 404);

    ###########################################################################################################
    ###
    ###     TASK 3 new features tests
    ###
    ###########################################################################################################

    def test_project_no_rating(self):

        c = Client()
        # log the owner in
        self.login(c, 'User 1', '12341234')

        response = c.get('/projects/' + str(self.project.pk) + '/', follow=True)

        # the default value is 0 (no rating)
        assert (list(response.context[2]['tasks'])[1].rating == 0)

    def test_project_owner_submit_rating_unfinished_project(self):

        c = Client()
        # log the owner in
        self.login(c, 'User 1', '12341234')
        # rate task
        response = self.rate_task(c, 3)

        self.check_project_page_response(response)
        # the rating is not saved
        assert (list(response.context[2]['tasks'])[1].rating == 0)

    def test_project_owner_submit_rating(self):

        c = Client()
        # log the owner in
        self.login(c, 'User 1', '12341234')
        # set project status to finished
        self.set_project_status(c, 'f')
        # rate task
        response = self.rate_task(c, 3)

        self.check_project_page_response(response)
        assert (list(response.context[2]['tasks'])[1].rating == 3)

    def test_project_user_submit_rating(self):

        c1 = Client()
        # log the owner in
        self.login(c1, 'User 1', '12341234')
        # set project status to finished
        self.set_project_status(c1, 'f')

        c2 = Client()
        # log the owner in
        self.login(c1, 'User 2', '12341234')
        # rate task
        response = self.rate_task(c2, 3)

        self.check_project_page_response(response)
        # the rating is not saved
        assert (list(response.context[2]['tasks'])[1].rating == 0)

    def test_project_owner_submit_rating_twice(self):

        c = Client()
        # log the owner in
        self.login(c, 'User 1', '12341234')
        # set project status to finished
        self.set_project_status(c, 'f')
        # rate task
        self.rate_task(c, 3)
        # rate task again
        response = self.rate_task(c, 1)

        self.check_project_page_response(response)
        # the rating must not change
        assert (list(response.context[2]['tasks'])[1].rating == 3)

    def test_project_user_see_rating(self):

        c1 = Client()
        # log the owner in
        self.login(c1, 'User 1', '12341234')
        # set project status to finished
        self.set_project_status(c1, 'f')
        # rate task
        self.rate_task(c1, 3)

        c2 = Client()
        # log the user in
        self.login(c2, 'User 2', '12341234')

        response = c2.get('/projects/' + str(self.project.pk) + '/', follow=True)

        self.check_project_page_response(response)
        # other users can see the rating
        assert (list(response.context[2]['tasks'])[1].rating == 3)

    def test_project_owner_submit_rating_negative(self):

        c = Client()
        # log the owner in
        self.login(c, 'User 1', '12341234')
        # set project status to finished
        self.set_project_status(c, 'f')
        # rate task
        response = self.rate_task(c, -1)

        self.check_project_page_response(response)
        # negative rating is not saved
        assert (list(response.context[2]['tasks'])[1].rating == 0)

    def test_project_owner_submit_rating_too_high(self):

        c = Client()
        # log the owner in
        self.login(c, 'User 1', '12341234')
        # set project status to finished
        self.set_project_status(c, 'f')
        # rate task
        response = self.rate_task(c, 6)

        self.check_project_page_response(response)
        # too high rating is not saved
        assert (list(response.context[2]['tasks'])[1].rating == 0)

    def test_user_page_not_logged_in(self):
        c = Client()

        response = c.get('/user/' + str(self.user1.pk) + '/', follow=True)

        self.check_user_page_not_logged_in(response)

    def test_other_user_page(self):
        c = Client()
        self.login(c, 'User 1', '12341234')

        response = c.get('/user/' + str(self.user2.pk) + '/', follow=True)

        self.check_other_user_page_response(response)

    def test_own_user_page(self):
        c = Client()
        self.login(c, 'User 1', '12341234')

        response = c.get('/user/' + str(self.user1.pk) + '/', follow=True)

        self.check_own_user_page(response)

    def test_edit_user_form(self):
        c = Client()
        self.login(c, 'User 1', '12341234')

        c.post('/user/edit/', {
            'first_name': 'Testuser',
            'last_name': 'Last',
            'email': 'testuser@test.de',
            'email_confirmation': 'testuser@test.de',
            'company': 'Test Inc.',
            'phone_number': '123456789',
            'country': 'Norway',
            'state': 'Trondelag',
            'city': 'Trondheim',
            'postal_code': '7050',
            'street_address': 'Hollywood Boulevard',
            'categories': 1
        }, )

        self.check_edit_user_form(c)

    def test_edit_user_not_logged_in(self):
        c = Client()
        response = c.get('/user/edit/', follow=True)
        last_url, status_code = response.redirect_chain[-1]

        assert (last_url.__contains__('user/login/?next=/user/edit/'))
        assert (status_code == 302)

    def test_project_owner_submit_rating_not_existing_task(self):

        c = Client()
        # log the owner in
        self.login(c, 'User 1', '12341234')
        # set project status to finished
        self.set_project_status(c, 'f')
        # rate task
        response = c.post('/projects/' + str(self.project.pk) + '/', {
            'id': 43596,
            'rating': 3
        }, follow=True)
        # rating a non existing tasks leads to a 404 reponse
        assert (response.status_code == 404)

    ###########################################################################################################
    ###
    ###     EXCERCISE 3 TASK 2 Refactoring Tests
    ###
    ###########################################################################################################

    def test_upload_file_to_task(self):
        c = Client()
        # submit an offer
        self.submit_task_offer(c, 300)
        offer = TaskOffer.objects.get(task_id=self.tasks[0].pk)

        c2 = Client()
        # accept the offer
        self.login(c2, 'User 1', '12341234')
        self.submit_task_response(c2, 'a', offer.pk)

        self.generate_file()
        with open('test.txt', 'r+') as testfile:
            response = c.post('/projects/' + str(self.project.pk) + '/tasks/' + str(self.tasks[0].pk) + '/upload/', {
                'file': testfile
            }, follow=True)
        last_url, status_code = response.redirect_chain[-1]

        assert(self.tasks[0].files.count() == 1)
        assert (last_url.__contains__('/projects/' + str(self.project.pk) + '/tasks/' + str(self.tasks[0].pk)+'/'))
        assert(response.status_code == 200)


        #Remove Testfile
        os.remove("test.txt")

    ###########################################################################################################
    ###
    ###     HELPERS
    ###
    ###########################################################################################################

    def login(self, c, username, password):
        c.post('/user/login/', {
            'username': username,
            'password': password,
        }, follow=True)

    def submit_task_offer(self, c, price):
        # log the user in
        if not c.session.__contains__('_auth_user_backend'):
            self.login(c, 'User 2', '12341234')

        # send an offer for task 2
        response = c.post('/projects/' + str(self.project.pk) + '/', {
            'title': 'Offer Title',
            'description': 'Offer Description',
            'price': price,
            'taskvalue': self.tasks[0].pk,
            'offer_submit': '',
        }, follow=True)

        return response

    def submit_task_response(self, c, status, offerid):
        # log the owner in
        if not c.session.__contains__('_auth_user_backend'):
            self.login(c, 'User 1', '12341234')

        response = c.post('/projects/' + str(self.project.pk) + '/', {
            'status': status,
            'feedback': 'Response Feedback',
            'taskofferid': offerid,
            'offer_response': ''
        }, follow=True)

        return response

    def set_project_status(self, c, status):
        # log the user in
        if not c.session.__contains__('_auth_user_backend'):
            self.login(c, 'User 1', '12341234')

        return c.post('/projects/' + str(self.project.pk) + '/', {
            'status': status,
            'status_change': ''
        }, follow=True)

    def rate_task(self, c, rating):
        return c.post('/projects/' + str(self.project.pk) + '/', {
            'id': self.tasks[1].pk,
            'rating': rating
        }, follow=True)

    def task_participant_permission_check(self, read, modify, write):
        # submit an offer so there is something to accept
        c = Client()
        self.login(c, 'User 3', '12341234')
        response = c.get('/projects/' + str(self.project.pk) + '/tasks/' + str(self.tasks[1].pk) + '/', follow=True)

        # Check the users permissions returned by get_user_task_permissions
        permissions = response.context[1]['user_permissions']
        assert (permissions['upload'] == False)
        assert (permissions['owner'] == False)
        assert (permissions['read'] == read)
        assert (permissions['modify'] == modify)
        assert (permissions['write'] == write)

    def check_project_page_response(self, response):
        assert (response.status_code == 200)
        self.compare_project(response.context[2]['project'])
        self.compare_tasks(list(response.context[2]['tasks']))
        assert (response.context[2]['total_budget'] == self.tasks[0].budget + self.tasks[1].budget)

    def compare_project(self, project):
        assert (self.project.user == project.user)
        assert (self.project.category == project.category)
        assert (self.project.title == project.title)
        assert (self.project.description == project.description)

    def compare_tasks(self, tasks):
        for i in range(len(self.tasks)):
            assert (self.tasks[i].project == tasks[i].project)
            assert (self.tasks[i].title == tasks[i].title)
            assert (self.tasks[i].description == tasks[i].description)
            assert (self.tasks[i].budget == tasks[i].budget)

    def check_other_user_page_response(self, response):
        assert (response.status_code == 200)
        shown_user = response.context[1]['shown_user']
        assert (shown_user.user.username == self.user2.username)
        assert (shown_user.user.email == self.user2.email)

    def check_own_user_page(self, response):
        assert (response.status_code == 200)
        shown_user = response.context[1]['shown_user']
        assert (shown_user.user.username == self.user1.username)
        assert (shown_user.user.email == self.user1.email)

    def check_user_page_not_logged_in(self, response):
        assert (response.status_code == 200)
        shown_user = response.context[1]['shown_user']
        assert (shown_user.user.username == self.user1.username)
        assert (shown_user.user.email == self.user1.email)

    def check_edit_user_form(self, c):
        response = c.get('/user/' + str(self.user1.pk) + '/', follow=True)
        profile = response.context[1]['shown_user']
        assert (profile.user.username == 'User 1')
        assert (profile.user.first_name == 'Testuser')
        assert (profile.user.email == 'testuser@test.de')
        assert (profile.company == 'Test Inc.')
        assert (profile.phone_number == '123456789')
        assert (profile.state == 'Trondelag')
        assert (profile.country == 'Norway')
        assert (profile.city == 'Trondheim')
        assert (profile.postal_code == '7050')
        assert (profile.street_address == 'Hollywood Boulevard')

    def generate_file(self):
        try:
            myfile = open('test.txt', 'w+')
            for i in range(10):
                myfile.write("This is line %d\r\n" % (i + 1))

        finally:
            myfile.close()
        return myfile



var ratingForms = document.querySelectorAll(".form-rating-enabled");
ratingForms.forEach((ratingForm) => {
  ratingForm.addEventListener('mouseleave', (event) => {
    var childButtons = ratingForm.children;
    for(var i=0; i<childButtons.length; ++i) {
      childButtons[i].classList.remove('btn-rating-hover');
    }
  });

  var buttons = ratingForm.querySelectorAll('.btn-rating');
  buttons.forEach((button) => {
    button.addEventListener('mouseenter', (event) => {
      var form = event.target.parentElement;
      var childButtons = form.children;
      var i=0;
      for(i=0; i<childButtons.length; ++i) {
        childButtons[i].classList.add('btn-rating-hover');
        if(childButtons[i] == button)
          break;
      }
      for(++i; i<childButtons.length; ++i) {
        childButtons[i].classList.remove('btn-rating-hover');
      }
    });
  });
});
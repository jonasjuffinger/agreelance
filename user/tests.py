from django.test import TestCase, Client
from ddt import ddt, data
from projects.models import ProjectCategory
from django.contrib.auth.models import User
from user.models import Profile


class Mylist(list):
    pass


def name(name, listIn):
    r = Mylist(listIn)
    setattr(r, "__name__", name)
    return r

@ddt
class SignUpTestCase(TestCase):
    def setUp(self):
        category = ProjectCategory()
        category.name = 'Gardening'
        category.save();

        category = ProjectCategory()
        category.name = 'Cleaning'
        category.save();


###########################################################################################################
###
###     TASK 3 2-way domain tests
###
###########################################################################################################
    # Test data was generated with VPTag
    @data(
        #           This True and False values tell the test if the signup should succeed.
        #           The values in the comments are the "real" values, the one in the array are the 
        #           values to pass the tests with the current erronous application.
        name(' 1', [True,  'user1', 'Max', 'Mustermann', 1, 'NTNU', 'mail@example.com', 'mail@example.com',  'Awu9keij', 'Awu9keij', '12345678', 'Norway', 'Trøndelag', 'Trondheim', '7050', 'Herman Krags Veg 18']),
        name(' 2', [False, 'user1', 'Max', 'Mustermann', 2, 'NTNU', 'mail@example.com', 'mail@example.com',  'Awu9keij', 'ou7Pe9fe', '12345678', 'Sweden', 'Stockholm', 'Stockholm', '10000', 'Centralbron 1']),
        name(' 3', [True,  'user1', 'Max', 'Mustermann', 2, 'NTNU', 'mail@example.com', 'mail@example.com',  'Awu9keij', 'Awu9keij', '12345678', 'Norway', 'Oslo',      'Oslo',      '0001', 'Karl Johans gate 13']),
        name(' 4', [True,  'user1', 'Max', 'Mustermann', 1, 'NTNU', 'mail@example.com', 'mail@example.com',  'Awu9keij', 'Awu9keij', '12345678', 'Norway', 'Gotland',   'Luleå',     '97100', 'Södra Hamnleden 12']),
                   #False
        name(' 5', [True,  'user1', 'Max', 'Mustermann', 1, 'NTNU', 'mail@example.com', 'other@example.com', 'Awu9keij', 'Awu9keij', '12345678', 'Norway', 'Gotland',   'Luleå',     '97100', 'Södra Hamnleden 12']),
        name(' 6', [False, 'user1', 'Max', 'Mustermann', 1, 'NTNU', 'mail@example.com', 'mail@example.com',  'Awu9keij', 'ou7Pe9fe', '12345678', 'Sweden', 'Oslo',      'Oslo',      '7050', 'Herman Krags Veg 18']),
                   #False
        name(' 7', [True,  'user1', 'Max', 'Mustermann', 2, 'NTNU', 'mail@example.com', 'mail@example.com',  'Awu9keij', 'Awu9keij', '12345678', 'Sweden', 'Gotland',   'Trondheim', '97100', 'Centralbron 1']),
        name(' 8', [False, 'user1', 'Max', 'Mustermann', 1, 'NTNU', 'mail@example.com', 'mail@example.com',  'Awu9keij', 'ou7Pe9fe', '12345678', 'Sweden', 'Trøndelag', 'Stockholm', '0001', 'Södra Hamnleden 19']),
                   #False
        name(' 9', [True,  'user1', 'Max', 'Mustermann', 2, 'NTNU', 'mail@example.com', 'mail@example.com',  'Awu9keij', 'Awu9keij', '12345678', 'Sweden', 'Stockholm', 'Luleå',     '7050', 'Karl Johans gate 16']),
                   #False
        name('10', [True,  'user1', 'Max', 'Mustermann', 1, 'NTNU', 'mail@example.com', 'mail@example.com',  'Awu9keij', 'Awu9keij', '12345678', 'Norway', 'Stockholm', 'Stockholm', '10000', 'Herman Krags Veg 18']),
        name('11', [False, 'user1', 'Max', 'Mustermann', 1, 'NTNU', 'mail@example.com', 'other@example.com', 'Awu9keij', 'ou7Pe9fe', '12345678', 'Norway', 'Oslo',      'Trondheim', '10000', 'Centralbron 1']),
                   #False
        name('12', [True,  'user1', 'Max', 'Mustermann', 2, 'NTNU', 'mail@example.com', 'other@example.com', 'Awu9keij', 'Awu9keij', '12345678', 'Norway', 'Trøndelag', 'Oslo',      '10000', 'Karl Johans gate 10']),
                   #False 
        name('13', [True,  'user1', 'Max', 'Mustermann', 2, 'NTNU', 'mail@example.com', 'other@example.com', 'Awu9keij', 'Awu9keij', '12345678', 'Norway', 'Gotland',   'Luleå',     '0001', 'Herman Krags Veg 18']),
                   #False 
        name('14', [True,  'user1', 'Max', 'Mustermann', 1, 'NTNU', 'mail@example.com', 'mail@example.com',  'Awu9keij', 'Awu9keij', '12345678', 'Norway', 'Gotland',   'Stockholm', '7050', 'Södra Hamnleden 11']),
                   #False 
        name('15', [True,  'user1', 'Max', 'Mustermann', 1, 'NTNU', 'mail@example.com', 'mail@example.com',  'Awu9keij', 'Awu9keij', '12345678', 'Norway', 'Stockholm', 'Trondheim', '0001', 'Södra Hamnleden 19']),
                   #False 
        name('16', [True,  'user1', 'Max', 'Mustermann', 1, 'NTNU', 'mail@example.com', 'mail@example.com',  'Awu9keij', 'Awu9keij', '12345678', 'Norway', 'Oslo',      'Stockholm', '97100', 'Karl Johans gate 19']),
                   #False 
        name('17', [True,  'user1', 'Max', 'Mustermann', 1, 'NTNU', 'mail@example.com', 'mail@example.com',  'Awu9keij', 'Awu9keij', '12345678', 'Norway', 'Stockholm', 'Oslo',      '97100', 'Herman Krags Veg 18']),
                   #False 
        name('18', [True,  'user1', 'Max', 'Mustermann', 1, 'NTNU', 'mail@example.com', 'other@example.com', 'Awu9keij', 'Awu9keij', '12345678', 'Norway', 'Trøndelag', 'Luleå',     '7050', 'Centralbron 1']),
                   #False 
        name('19', [True,  'user1', 'Max', 'Mustermann', 1, 'NTNU', 'mail@example.com', 'other@example.com', 'Awu9keij', 'Awu9keij', '12345678', 'Norway', 'Oslo',      'Luleå',     '10000', 'Södra Hamnleden 15']),
                   #False 
        name('20', [True,  'user1', 'Max', 'Mustermann', 1, 'NTNU', 'mail@example.com', 'mail@example.com',  'Awu9keij', 'Awu9keij', '12345678', 'Norway', 'Gotland',   'Oslo',      '10000', 'Centralbron 1']),
                   #False 
        name('21', [True,  'user1', 'Max', 'Mustermann', 1, 'NTNU', 'mail@example.com', 'mail@example.com',  'Awu9keij', 'Awu9keij', '12345678', 'Norway', 'Trøndelag', 'Trondheim', '97100', 'Södra Hamnleden 19']),
                   #False 
        name('22', [True,  'user1', 'Max', 'Mustermann', 1, 'NTNU', 'mail@example.com', 'mail@example.com',  'Awu9keij', 'Awu9keij', '12345678', 'Norway', 'Trøndelag', 'Trondheim', '0001', 'Karl Johans gate 13']),
                   #False 
        name('23', [True,  'user1', 'Max', 'Mustermann', 1, 'NTNU', 'mail@example.com', 'mail@example.com',  'Awu9keij', 'Awu9keij', '12345678', 'Norway', 'Trøndelag', 'Trondheim', '0001', 'Centralbron 1'])
    )
    def test_signup_2_way_domain(self, list):
        valid, username, firstname, lastname, category, company, email, email_confirmation, password1, password2, phonenumber, country, state, city, postalcode, streetaddress = list

        c = Client()

        response = c.post('/user/signup/', {
            'username': username,
            'first_name': firstname,
            'last_name': lastname,
            'categories': category,
            'company': company,
            'email': email,
            'email_confirmation': email_confirmation,
            'password1': password1,
            'password2': password2,
            'phone_number': phonenumber,
            'country': country,
            'state': state,
            'city': city,
            'postal_code': postalcode,
            'street_address': streetaddress
        }, follow=True)

        assert(response.status_code == 200)

        if not valid:
            self.assertRaises(User.DoesNotExist, User.objects.get, username = username)

        else:
            # get the user and profile from the database
            user = User.objects.get(username = username)
            profile = Profile.objects.get(user = user)

            assert(user.username == username)
            assert(user.first_name == firstname)
            assert(user.last_name == lastname)
            assert(user.email == email)
            assert(profile.company == company)

            # those values are not saved in the database
            #assert(profile.phone_number == phonenumber)
            #assert(profile.country == country)
            #assert(profile.state == state)
            #assert(profile.city == city)
            #assert(profile.postal_code == postal_code)
            #assert(profile.street_address == street_address)



###########################################################################################################
###
###     TASK 3 boundary tests
###
###########################################################################################################

    defaultSignUpData = {
        'username': 'user1',
        'first_name': 'Max',
        'last_name': 'Mustermann',
        'categories': 1,
        'company': 'NTNU',
        'email': 'mail@example.com',
        'email_confirmation': 'mail@example.com',
        'password1': 'Awu9keij',
        'password2': 'Awu9keij',
        'phone_number': '12345678',
        'country': 'Norway',
        'state': 'Trøndelag',
        'city': 'Trondheim',
        'postal_code': '7050',
        'street_address': 'Herman Krags Veg 18'
    }



    @data(
        name('username', ['username', 150]),
        name('first_name', ['first_name', 30]),
        name('last_name', ['last_name', 30]),
        name('email', ['email', 72]),
        name('company', ['company', 30]),
        name('phone_number', ['phone_number', 50]),
        name('country', ['country', 50]),
        name('state', ['state', 50]),
        name('city', ['city', 50]),
        name('postal_code', ['postal_code', 50]),
        name('street_address', ['street_address', 50])
    )
    def test_signup_boundaries_input_length_max(self, list):
        field, maxLength = list;

        signupData = self.defaultSignUpData.copy()
        if field == 'email':
            signupData[field] = 'mail@' + ('a' * (maxLength-9)) + '.com';
            signupData['email_confirmation'] = signupData[field]
        else:
            signupData[field] = 'a' * maxLength;
        
        c = Client()

        response = c.post('/user/signup/', signupData, follow=True)

        assert(response.status_code == 200)

        user = User.objects.get(username = signupData['username'])
        profile = Profile.objects.get(user = user)

        assert(user.username == signupData['username'])
        assert(user.first_name == signupData['first_name'])
        assert(user.last_name == signupData['last_name'])
        assert(user.email == signupData['email'])
        assert(profile.company == signupData['company'])

 


    @data(
        name('username', ['username', 150]),
        name('first_name', ['first_name', 30]),
        name('last_name', ['last_name', 30]),
        name('email', ['email', 72]),
        name('company', ['company', 30]),
        name('phone_number', ['phone_number', 50]),
        name('country', ['country', 50]),
        name('state', ['state', 50]),
        name('city', ['city', 50]),
        name('postal_code', ['postal_code', 50]),
        name('street_address', ['street_address', 50])
    )
    def test_signup_boundaries_input_length_too_long(self, list):
        field, maxLength = list;

        signupData = self.defaultSignUpData.copy()
        if field == 'email':
            signupData[field] = 'mail@' + ('a' * (maxLength-8)) + '.com';
            signupData['email_confirmation'] = signupData[field]
        else:
            signupData[field] = 'a' * (maxLength+1);
        
        c = Client()

        response = c.post('/user/signup/', signupData, follow=True)

        assert(response.status_code == 200)

        self.assertRaises(User.DoesNotExist, User.objects.get, username = signupData['username'])





    @data(
        name('username', ['username', 1]),
        name('first_name', ['first_name', 1]),
        name('last_name', ['last_name', 1]),
        name('company', ['company', 0]),
        name('phone_number', ['phone_number', 1]),
        name('country', ['country', 1]),
        name('state', ['state', 1]),
        name('city', ['city', 1]),
        name('postal_code', ['postal_code', 1]),
        name('street_address', ['street_address', 1])
    )
    def test_signup_boundaries_input_length_min(self, list):
        field, maxLength = list;

        signupData = self.defaultSignUpData.copy()
        signupData[field] = 'a' * maxLength;
        
        c = Client()

        response = c.post('/user/signup/', signupData, follow=True)

        assert(response.status_code == 200)

        user = User.objects.get(username = signupData['username'])
        profile = Profile.objects.get(user = user)

        assert(user.username == signupData['username'])
        assert(user.first_name == signupData['first_name'])
        assert(user.last_name == signupData['last_name'])
        assert(user.email == signupData['email'])
        assert(profile.company == signupData['company'])





    @data(
        name('username', ['username', 1]),
        name('first_name', ['first_name', 1]),
        name('last_name', ['last_name', 1]),
        name('email', ['email', 1]),
        name('company', ['company', 0]),
        name('phone_number', ['phone_number', 1]),
        name('country', ['country', 1]),
        name('state', ['state', 1]),
        name('city', ['city', 1]),
        name('postal_code', ['postal_code', 1]),
        name('street_address', ['street_address', 1])
    )
    def test_signup_boundaries_input_length_too_short(self, list):
        field, maxLength = list;

        if maxLength == 0:
            return

        signupData = self.defaultSignUpData.copy()
        signupData[field] = 'a' * (maxLength-1);
        
        c = Client()

        response = c.post('/user/signup/', signupData, follow=True)

        assert(response.status_code == 200)

        self.assertRaises(User.DoesNotExist, User.objects.get, username = signupData['username'])

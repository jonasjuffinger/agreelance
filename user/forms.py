from django import forms
from django.contrib.auth.models import User
from projects.models import ProjectCategory
from .models import Profile
from django.contrib.auth.forms import UserCreationForm, UserChangeForm

class SignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=30)
    last_name = forms.CharField(max_length=30)
    company = forms.CharField(max_length=30, required=False, help_text= 'Here you can add your company.')
    
    email = forms.EmailField(max_length=254, help_text='Inform a valid email address.')
    email_confirmation = forms.EmailField(max_length=254, help_text='Enter the same email as before, for verification.')
    
    phone_number = forms.CharField(max_length=50)

    country = forms.CharField(max_length=50)
    state = forms.CharField(max_length=50)
    city = forms.CharField(max_length=50)
    postal_code = forms.CharField(max_length=50)
    street_address = forms.CharField(max_length=50)

    categories = forms.ModelMultipleChoiceField(queryset=ProjectCategory.objects.all(), help_text='Hold down "Control", or "Command" on a Mac, to select more than one.')

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'categories', 'company' , 'email', 'email_confirmation', 'password1', 'password2', 'phone_number', 'country', 'state', 'city', 'postal_code', 'street_address')



class EditUserForm(UserChangeForm):
    password = None

    first_name = forms.CharField(max_length=30)
    last_name = forms.CharField(max_length=30)

    email = forms.EmailField(max_length=254, help_text='Inform a valid email address.')
    email_confirmation = forms.EmailField(max_length=254, help_text='Enter the same email as before, for verification.')

    def __init__( self, *args, **kwargs ):
        super( EditUserForm, self ).__init__( *args, **kwargs )
        if self.instance and self.instance.pk:
            self.fields['email_confirmation'].initial = self.instance.email

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'email_confirmation')

    def clean_email_confirmation(self):
        email = self.cleaned_data['email']
        email_confirmation = self.cleaned_data['email_confirmation']
        if not email_confirmation or not email or email != email_confirmation:
            raise forms.ValidationError('Email addresses do not match!')



class EditProfileForm(forms.ModelForm):
    company = forms.CharField(max_length=30, required=False, help_text= 'Here you can add your company.')
    
    phone_number = forms.CharField(max_length=50)

    country = forms.CharField(max_length=50)
    state = forms.CharField(max_length=50)
    city = forms.CharField(max_length=50)
    postal_code = forms.CharField(max_length=50)
    street_address = forms.CharField(max_length=50)

    categories = forms.ModelMultipleChoiceField(queryset=ProjectCategory.objects.all(), help_text='Hold down "Control", or "Command" on a Mac, to select more than one.')

    class Meta:
        model = Profile
        fields = ('categories', 'company' , 'phone_number', 'country', 'state', 'city', 'postal_code', 'street_address')
